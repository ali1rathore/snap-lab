.. include:: ../README.rst


# Building `snaplab` executable

##### Linux dependancies

* Install OS level python and thrift sasl dependancies:

        apt-get update && apt-get install libsasl2-modules libsasl2-dev libssl-dev libffi-dev python3 python3-pip

* Install Python packages:

        python3 -m pip install -r requirements.txt

##### MacOS dependancies

* Make sure Python 3 is installed
* Use `conda` or `pip` to install requirements:

        pip install -r requirements.txt

### Creating the single-file executable

Run the `make executable` command

The binary will be in `dist/snaplab-linux` or `dist/snaplab-darwin`

### Publish `snaplab` executable to gitlab

### Upload to the repo

```
curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --form "file=@dist/snaplab-darwin" https://gitlab.com/api/v4/projects/7296632/uploads
```

You'll get a response like this:

```json
{
"alt":"snaplab-darwin"
,"url":"/uploads/c8fc3d0c5222d95748ff81f25b4b15bf/snaplab-darwin"
,"markdown":"[snapl-darwin](/uploads/c8fc3d0c5222d95748ff81f25b4b15bf/snaplab-darwin)"
}
```

### Create new `snaplab` release

Add the `markdown` returned when uploading the executable to the README.md


## Publish SnapCube to Artifactory

```bash
curl -X PUT -F name=somevalue -H "Expect:"  -i -u <user>:<password> -vvv -T </path/to/snapcube.snapcube> "https://artifacthub.oraclecorp.com/snap-compute-bdcsce-local/snapcubes/<target>.snapcube"
```
