 SNAP Lab
========= 

Tools for developing, testing, and demoing Snap apps

[![pipeline status](https://gitlab.com/SparklineData/Public/snap-lab/badges/master/pipeline.svg)](https://gitlab.com/SparklineData/Public/snap-lab/commits/master) | [![Documentation Status](//readthedocs.org/projects/snap-lab/badge/?version=latest)](https://snap-lab.readthedocs.io/en/latest/?badge=latest)

## Quickstart

1. Install SNAP Lab (see next section)

2. Create a local SNAP Cluster (configuration directory should not yet exist):

        snap_lab cluster create /path/for/new/cluster/configuration/directory
        
3. Start the notebook server to get started

        export SNAP_CLUSTER_DIR=/path/for/new/cluster/configuration/directory
        snap_lab cluster notebook
        
4. Now follow the links to get started

## Install SNAP Lab

On all platforms, you'll first need Python 3.6, Docker, and Docker Compose

* Installing [Docker](https://docs.docker.com/install/)
* Installing [Docker Compose](https://docs.docker.com/compose/install/)

> **Ubuntu/Debian:** You'll need sasl and python dependancies:  
>   `apt-get update && apt-get install libsasl2-modules libsasl2-dev libssl-dev libffi-dev python3 python3-pip`  

> **Mac:** We recommend installing [Anaconda](https://www.anaconda.com/download) to install Python 3.6

You can install the latest `snap_lab` module and command-line using `pip` or `conda`:

```bash
pip install git+https://gitlab.com/SparklineData/Public/snap-lab.git
```

#### SNAP Command Line Tool

You should now have `snap_lab` command in your PATH.

```python
$ snap_lab

Command line tools for interacting
    with SNAP clusters

    cluster - create, start, stop a cluster
    sql - connect to a SNAP cluster and run sql
    box - download and install snap packages
    
Usage:       snap_lab 
             snap_lab box
             snap_lab cluster
             snap_lab sql
```

#### SNAP Python module

You can also use the `snap_lab` module in your Python code

```python
import snap_lab

cluster = snap_lab.SNAPCluster("/path/to/cluster/dir")
cluster.start()

client = snap_lab.SNAPClient("localhost")
client.sql("show tables")

cluster.stop()
```

Download `snap_lab` executable only:
-------------------------


If you dont want to setup Python 3.6, try these pre-built executables  
for the SNAP command line tool.

> **Warning:** These are not automatically updated and may no longer work


| OS | Link 
|:----:|:------:|
| Linux | [snapl-linux](/uploads/23def2d97f9c73cb0767d0850f355646/snapl-linux) |
| Mac | [snapl-darwin](/uploads/c8fc3d0c5222d95748ff81f25b4b15bf/snapl-darwin) |
| Windows | by request only |


Info
----

* Free software: Apache Software License 2.0
* Documentation: https://snap-lab.readthedocs.io.

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

* [Cookiecutter](https://github.com/audreyr/cookiecutter)
* [audreyr/cookiecutter-pypackage](https://github.com/audreyr/cookiecutter-pypackage)
