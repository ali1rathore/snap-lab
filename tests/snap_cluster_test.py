import os, pytest, time
import snap_lab.snap_cluster as snap_cluster
import snap_lab.snap_client as snap_client
@pytest.fixture
def cluster(capsys):


    # Start the cluster
    assert os.path.exists(os.environ["SPARK_HOME"])
    assert os.path.exists(os.environ["SNAP_HOME"])
    assert os.path.exists(os.environ["SHARED_DIR"])

    # Create temporary directory
    # tmpdir = tmpdir_factory.mktemp("data")
    # cluster_dir = os.path.join(tmpdir.dirname,"dummy")
    cluster_dir = os.path.join(os.getcwd(),"build","testing","test_cluster")
    if not os.path.exists(cluster_dir):
        print("Creating new cluster in {}".format(cluster_dir))
        snap_cluster.SNAPCluster().create(dir=cluster_dir)

    assert os.path.exists(cluster_dir)

    cluster = snap_cluster.SNAPCluster(dir=cluster_dir)

    cluster.start(scale=3)

    # wait until cluster is ready for connection
    snap_client.SNAPClient("localhost",wait=20,retries=6)

    yield cluster

    print("Stopping cluster in 15 seconds")
    time.sleep(15)

    print("Stopping cluster {}".format(cluster))
    cluster.stop()

def test_run_sql(cluster):
    cluster.status()
    snap = snap_client.SNAPClient("localhost")
    snap.sql("snap version")
    assert snap is not None
