import os, pytest, time
import snap_lab.snap_lab as snap_lab
from snap_lab.snap_lab import SNAPLab
from snap_lab import SNAPBox
from snap_lab import SNAPClient

SNAP_HOST="localhost"
SNAP_PORT=10000
SNAP_HOST="192.168.64.5"
SNAP_PORT=32133
BOX_NAME="/Users/aarathor/code/ssbenchmark"

def test_get_databases():
    print(SNAPClient(SNAP_HOST,SNAP_PORT).sql("show databases"))

def test_demo():
    snap_lab.demo()

def test_build():

    box = SNAPBox(dir=BOX_NAME)

    box.build()

def test_download():

    box = SNAPBox(dir=BOX_NAME)
    box.download("ssbenchmark.snapcube")


def test_install():
    """ Installing a test box """
    box = SNAPBox(dir=BOX_NAME)
    client = SNAPClient(SNAP_HOST, SNAP_PORT)

    if snap_lab.is_installed(box, client):
        snap_lab.uninstall(box, client)


    SNAPLab().install(BOX_NAME, SNAP_HOST, SNAP_PORT
                      , data_path="/data/shared/ssbenchmark"
                      , indexes=True)

    assert snap_lab.is_installed(box, client)

def test_indexing():

    box = SNAPBox(dir=BOX_NAME)
    client = SNAPClient(SNAP_HOST, SNAP_PORT)

    print("Starting Indexing...this might take a while")

    SNAPLab().reindex(BOX_NAME, SNAP_HOST, SNAP_PORT)

    print("Checking indexes are not empty")

    db = os.path.basename(BOX_NAME)
    client.sql(f"use {db}")
    for index in box._insert_indexes.keys():
        r = client.sql("select count(*) as count from {}".format(index))
        number_of_rows_in_index = r[0]['count']

        assert number_of_rows_in_index != 0

def test_build_parquet():
    box = SNAPBox(dir=BOX_NAME)
    client = SNAPClient(SNAP_HOST, SNAP_PORT,database="ssbenchmark")
    client.sql("drop database if exists ssbenchmark_pqt cascade ")
    snap_lab.create_parquet_tables(client,"ssbenchmark","ssbenchmark_pqt")

def test_queries():

    box = SNAPBox(dir=BOX_NAME)
    client = SNAPClient(SNAP_HOST,SNAP_PORT,database=os.path.basename(BOX_NAME))
    import canonicaljson, hashlib
    result_hashes = {}
    for n,q in box.queries.items():
        print("Running query {}".format(n))
        r = client.sql(q)
        canonical_result = canonicaljson.encode_canonical_json(r)
        digest = hashlib.md5(canonical_result).hexdigest()
        result_hashes[n] = digest

    expected_result_hashes =  box._get_results_hash_dict()
    if expected_result_hashes:
        for query_name in result_hashes:
            if query_name in expected_result_hashes:
                if not result_hashes[query_name] ==  expected_result_hashes[query_name]:
                    print("Result for query {} dont match expected".format(query_name))
                assert result_hashes[query_name] ==  expected_result_hashes[query_name]
            else:
                print("Warning, no expected result hashes for  query '{}' found".format(query_name))
    else:
        print("Writing result hashes")
        box._write_result_hash_dict(result_hashes)

def test_uninstall():

    box = SNAPBox(dir=BOX_NAME)
    client = SNAPClient(SNAP_HOST, SNAP_PORT)

    if not snap_lab.is_installed(box, client):
        snap_lab.install(box, client)
    snap_lab.uninstall(box, client)

    assert not snap_lab.is_installed(box, client)


