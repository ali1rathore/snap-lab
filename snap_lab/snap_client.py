#!/usr/bin/env python3
import fire
from pyhive import hive
import logging

class SNAPClient(object):
    """SNAP SQL Client
    Features:
        * Query parameters
        * Explain Plan and automatic limit
        * Better errors
    """

    SNAP_PORT=10000

    def __repr__(self):
        return f"SnapClient@{self._host}:{self._port}"

    def __init__(self,host
                 ,port=SNAP_PORT
                 ,auth="NONE", database=""
                 ,wait=10,retries=6):

        self._logger = logging.getLogger(name="SNAPClient[{}:{}]".format(host,port))

        self._host = host
        self._port = port

        self._conn = None
        err = None
        for i in range(retries + 1):
            self._conn,err = self._get_snap_connection(host, port, auth, database)
            if self._conn:
                break
            else:
                import time
                self._logger.warning("Tried {} times to connect to spark, waiting {} more secs".format(i + 1, wait))
                time.sleep(wait)

        if not self._conn:
            raise Exception("Unable to Connect to {}:{} : {}".format(host,port,err))

        self._cursor = None

    def close(self):
        """
        Close the cursor and connection if they exist
        :return: None
        """
        try:
           self._cursor.close
        except:
            pass

        try:
            self._conn.close()
        except:
            pass


    def __del__(self):
        self.close()

    def database_exists(self,dbname):
        result = self.sql("show databases")
        for row in result:
            if row["databaseName"] == dbname:
                return True
        return False

    def get_database_info(self,dbname):
        if self.database_exists(dbname):
            return self.sql("describe database extended {}".format(dbname))
        return None

    def get_dbparameters(self,dbname):
        result = self.get_database_info(dbname)
        if result:
            for row in result:
                if row["database_description_item"] == "Properties":
                    params = row["database_description_value"]
                    return params
        return result

    def sql(self,sql,explain=False,limit=100,kwargs={},result_type='json'):
        """
        Run a SQL query
        :param sql: query text
        :param explain: run explain plan instead
        :param limit: limit row results (default=100)
        :param kwargs: parameters for query text
        :return: query results
        """
        if kwargs:
            sql = sql.format(**kwargs)

        is_select = sql.lstrip().lower().startswith('select')
        has_limit = ("limit" in sql.lower()) and (sql.split(" ")[-2].lower() == "limit")

        if explain:
            sql = "explain " + sql
        if (limit != 0) and (not has_limit) and is_select:
            sql = sql + " limit " + str(limit)

        self._logger.debug('Running SQL: {}'.format(sql))

        sqls = sql.split(';')
        for s in sqls:
            r,e = self._sql(s)
            if e:
                self._logger.error("""Error while running SQL:\n\n{msg}""".format(msg=e['message']))

        if result_type == 'dataframe':
            import pandas
            r = pandas.DataFrame.from_dict(r)

        return r

    def show_segments(self,indexName):
        return self.sql("""select hostpriority, host, count(ss.host)
                    from `snap$segcachemanagersegments` as ss 
                    where indexName={indexName}
                    group by host, hostpriority
                    order by hostpriority,count(host)""".format(indexName=indexName))

    def _host_segments(self,indexName):
        import pandas
        r = self.sql('select * from `snap$segcachemanagersegments` where indexName={}'.format(indexName), limit=10000)
        segments_info = pandas.DataFrame.from_dict(r)
        host_segments = {}
        for r in segments_info.values:
            host = r[0]
            hostpriority = r[1]
            segmentName = r[4]
            if host not in host_segments:
                host_segments[host] = {}
            host_segments[host][segmentName] = hostpriority
        df = pandas.DataFrame.from_dict(host_segments)
        return df

    def segment_heatmap(self, indexName, **kwargs):
        import seaborn as sns
        from matplotlib import pyplot
        segments = self._host_segments(indexName)
        pyplot.figure(figsize=(10, 16))
        return sns.heatmap(segments, **kwargs)

    def get_partition_columns(self,fqTableName):
        """
        Returns list of names of partitioning colums
        """

        df = self.sql(f"describe extended {fqTableName}", result_type="dataframe")
        partition_columns = []
        partition_information_index = df.index[df["col_name"] == "# Partition Information"]
        if not partition_information_index.empty:
            empty_row_index = df.index[df["col_name"] == ""]
            partition_info = df[partition_information_index[0] + 2: empty_row_index[0]]
            partition_columns = partition_info['col_name'].tolist()
        return partition_columns

    def get_table_location(self,tableName):
        """
        Return the file location of a table or None
        """
        df = self.sql("describe extended {tableName}".format(tableName=tableName), result_type="dataframe")
        location_index = df.index[df["col_name"] == "Location"]
        location = df.loc[location_index]['data_type'].tolist()
        return location[0] if location else None

    def _sql(self, sql):
        """
        Executes a query and returns a pair of (result,error).
        if error is not None, then result is valid.
        :param sql: sql query text
        :return: (Result, Error)
        """
        if not self._cursor:
            self._cursor = self._conn.cursor()

        error = None
        result = None
        try:
            self._cursor.execute(sql)
            header = list(map(lambda l: l[0], self._cursor.description))
            rows = self._cursor.fetchall()
            result = list(map(lambda r: dict(zip(header, r)), rows))
        except Exception as e:
            error = self._handle_exception(e)
        finally:
            self._cursor.close()
            self._cursor = None

        return result, error

    def _get_snap_connection(self,host,port,auth,database):
        conn = None
        err = None
        if not database:
            database = "default"
        try:
            if auth == "NONE":
                conn = hive.Connection(host=host,port=int(port),database=database)
            elif auth == "NOSASL":
                conn = hive.Connection(host=host,port=int(port),auth="NOSASL",database=database)
            else:
                self._logger.error("invalid authentication: " + auth)
        except Exception as e:
            err = self._handle_exception(e)

        return conn,err

    def _handle_exception(self,e):
        msg = e.args[0]
        trace = None
        if not isinstance(e.args[0],str):
            msg = e.args[0].status.errorMessage
            trace = e.args[0].status.infoMessages
        return  {"message":msg,"stacktrace":trace, 'error':e}



if __name__ == '__main__':
    fire.Fire(SNAPClient)
