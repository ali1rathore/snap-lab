<!-- markdownlint-disable MD033 -->


# Snap Lab

Snap Lab helps you
 * set up a snap cluster
 * download pre-packaged datasets
 * query and analyse the indexed data


<section-start>

```python
from IPython.display import display, Markdown
import os
from snap_lab import SNAPCluster

snap = None

notebook = True
monitoring = False
snapserver = True
compiler=False
workers=3

SNAP_CLUSTER_DIR=os.getenv("SNAP_CLUSTER_DIR","")
SNAP_HOME=os.getenv("SNAP_HONE","")
SHARED_DIR=os.getenv("SHARED_DIR","")


```

</section-start>

### A fist full of configs

We need to know about a few directories first:
<section-live>

* which directory to use for storing the snap cluster configuration: 
    <variable-string>SNAP_CLUSTER_DIR</variable-string>
* Where did you set as SNAP_HOME:  
    <variable-string>SNAP_HOME</variable-string>
* which directory to be mounted to `/shared` on all containers: 
    <variable-string>SHARED_DIR</variable-string>
</section-live>

# Create and start the cluster! 

The first time we start the cluster, we need to "build" and "pull"
some container images.  So this may take a while.

`snapl cluster create /path/to/new/cluster/dir`

This will create the directory and download the default 
configuration.

You now ready to start the cluster

```bash
snapl cluster start
```

#### Starting A Multi-Container Snap cluster
> You can scale up and down the number of spark nodes using the `--scale` flag

<section-live>

<variable-tick>snapserver</variable-tick>
<variable-tick>notebook</variable-tick>
<variable-tick>monitoring</variable-tick> 
<variable-tick>compiler</variable-tick>

<variable-slider label="Number of Workers" min="1" max="10" step="1">workers</variable-slider>

```python

containers = ["master","slave"]
if notebook:
    containers.append("notebook")
if snapserver:
    containers.append("snap")
if monitoring:
    containers += ["telegraf","grafana","influxdb"]
if compiler:
    containers.append("compiler")
    
display(Markdown("""
`snapl cluster start --scale={workers} {containers}`
""".format(workers=workers,containers=' '.join(containers))))

```

</section-live>

Press the button to create the cluster:

<section-button inline value="Start Cluster">

```python
display(Markdown("""
## Creating Cluster  
* SNAP_CLUSTER_DIR = {SNAP_CLUSTER_DIR}
* SHARED_DIR = {SHARED_DIR}
* SNAP_HOME = {SNAP_HOME}
* SPARK_HOME = {SPARK_HOME}
""".format(**globals())))

from snaplib.snap_cluster import SNAPCluster
cluster = SNAPCluster(dir=SNAP_CLUSTER_DIR)
import os
if not os.path.exists(SNAP_CLUSTER_DIR):
    print("Creating new cluster directory")
    cluster.create(dir=SNAP_CLUSTER_DIR)

print("Starting  cluster")

os.environ["SHARED_DIR"] = SHARED_DIR
os.environ["SNAP_HOME"] = SNAP_HOME


r = cluster.start(*containers,scale=workers)
print(r)
```

<section-button inline value="Cluster Status!">

```python
cluster = SNAPCluster(dir=SNAP_CLUSTER_DIR)
cluster.status()
```
</section-button>


</section-button>

# Run some SQL:

Enter a SQL Query: <variable-string>query</variable-string>

<section-button inline value="Execute!">

```python
display(Markdown("""
## Running Query :

{query}

### Result: 

""".format(query=query)))

if not snap:
    from snap_lab import SNAPClient
    snap = SNAPClient("localhost")

result = snap.sql(query,result_type='dataframe')
```

</section-button>

<section-live>
<variable-table>result</variable-table>
</section-live>

# See the cluster logs

You can see the logs of any of the contains by doing:

`snapl cluster logs [snap] [master] [notebook]`

> The executor logs will not be shown in the container logs.
> Executor logs will be in `$SPARK_HOME/work/`

# Stop the cluster

You can stop the cluster with

`snapl cluster stop`

<section-button inline value="Stop Em">

```python
cluster.stop()
```

</section-button>
