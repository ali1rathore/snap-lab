#!/usr/bin/env python3
import os
import fabric
import shutil
import glob
import snap_lab.utils as utils
import logging
import invoke
"""
snapl cluster create /path/to/mycluster
snapl cluster start [/path/to/mycluster]
snapl cluster status
snapl cluster logs

snapl cube install /path/to/my/snapcube --snap_host=snap --snap_port=10000
snapl cube verify /path/to/my/snapcube --snap_host=snap --snap_port=10000
snapl cube ddl /path/to/my/snapcube

snapl sql --from-cube /path/to/my/snapcube --snap_host=snap --snap_port=10000
snapl cube publish /path/to/my/snapcube
snapl sql show tables
snapl cube uninstall /path/to/my/snapcube
snapl cluster stop /path/to/my/cluster

snapl config upgrade-jar
snapl config edit
"""


SPARK_LAB_ORG_URL="https://gitlab.com/ali1rathore//"
SPARK_LAB_REPO_NAME="spark-lab"
SPARK_LAB_BRANCH="master"

SNAP_CLUSTER_DIR="SNAP_CLUSTER_DIR"

class SNAPCluster(object):
    """ Manage a local running snap cluster
        using Docker Compose
        and docker-compose.yaml files

        Each cluster is its on directory of compose files
        Create a new cluster with the `init` command
        Start/stop clusters
     """


    _COMPOSE_ENV_VARS_ = {
        "SPARK_VERSION" : "2.2.1"
        ,"SNAP_PORT" : "10000"
        ,"SNAP_HOST" : "localhost"
        ,"WORKER_CORES" : "1"
        ,"WORKER_MEMORY" : "1g"
    }

    def create(self,dir):
        """Setup a new SNAP Cluster configuration directory"""

        if os.path.exists(dir):
            raise IsADirectoryError("Desired directory already exists: {}".format(dir))

        self._logger.info("""
            Fetching spark-lab files 
                from repo : {url}/{repo}/{branch} 
                to directory : {dir}
            """.format(url=SPARK_LAB_ORG_URL,repo=SPARK_LAB_REPO_NAME,branch=SPARK_LAB_BRANCH,dir=dir))

        utils.fetch_gitlab_tar(SPARK_LAB_ORG_URL,SPARK_LAB_REPO_NAME,SPARK_LAB_BRANCH,dir)

        self._logger.info("""
            Downloaded spark-lab files, now:
                * adding 'snap-server' docker-compose files
                * adding 'snap-notebooks' ipynb files
            """)

        self._create_snap_docker_compose_dir(os.path.join(dir,"snap-server"))
        self._create_snap_notebooks_dir(os.path.join(dir,"documents","notebooks","snap-notebooks"))
        self._create_snap_scriptedforms_dir(os.path.join(dir,"documents","scriptedforms","snap-scriptedforms"))

        self._logger.info("""
            Finished creating new SNAP cluster directory : {dir}
            
            To automatically use this cluster, set the {var} variable to {dir}
            
                export {var}={dir}
                
            For help getting stared, try the notebook container:
            
                snap_lab cluster notebook
                
        """.format(dir=dir,var=SNAP_CLUSTER_DIR))


    def __init__(self,dir=""):
        """
        Manage a cluster based on a specific directory
        """

        # the connection to localhost for calling shell commands
        # We'll create it on the fly
        self._fabric_connection = None

        # directory container the docker-compose files
        self._cluster_directory = dir
        if not self._cluster_directory:
            self._cluster_directory = os.getenv(SNAP_CLUSTER_DIR,"")

        self._logger = logging.getLogger("SNAPCluster[{}]".format(self._cluster_directory))

    @property
    def _dir(self):
        if not self._cluster_directory:
            self._cluster_directory = os.getenv(SNAP_CLUSTER_DIR,"")
        return self._cluster_directory

    def status(self):
        """
        List the containers in running cluster
        """
        self._do_compose('ps')

    def start(self,*containers, scale=1):
        """
        Bring up the cluster
        :param containers: which containers to bring up
        """

        self._do_compose( "up -d --scale slave={scale} {containers}".format(
            scale=scale,containers=' '.join(containers)
        ))

    def notebook(self):
        self.start("notebook")
        import time; time.sleep(1)
        import webbrowser
        url="http://0.0.0.0:8888/nteract/edit/snap-notebooks/Index.ipynb"
        self._logger.info("Opening browser for notebook at {}".format(url))
        webbrowser.open(url)

    def stop(self):
        """
        Shutdown cluster
        """
        self._do_compose("down")

    def logs(self,*containers):
        """
        Show container logs
        """

        self._do_compose("logs --tail 100 -f {}".format(' '.join(containers)))

    def restart(self,*containers):
        """
        Restart Containers
        :param containers: which containers to restart
        :return: None
        """
        self._do_compose("restart {}".format(' '.join(containers)))

    def rm(self,*containers):
        """
        Delete Containers
        :param containers: which containers
        """
        self._do_compose("rm {}".format(' '.join(containers)))

    def _do_compose(self, command):

        if not self._cluster_directory:
            self._cluster_directory = utils.getenv_or_exit(SNAP_CLUSTER_DIR, """
                   Please set the {} environment variable to the desired snap cluster directory
                   """)

        if not self._check_dependancies():
            utils.do_exit(msg="Failed to check all dependancies")


        config_vars = self._check_configs()
        if "PATH" in os.environ:
            config_vars["PATH"] = os.environ["PATH"]

        for d in  ["SNAP_HOME","SHARED_DIR"]:
            if not os.path.exists(config_vars[d]):
                utils.do_exit(msg="The directory for {} ({}) does not exist".format(d,config_vars[d]))

        env_vars = ' '.join(["{k}={v}".format(k=k,v=v) for (k,v) in config_vars.items()])
        compose_files_flags = ' -f '.join(self._compose_files.values())

        docker_comose_cmd = """{env_vars} docker-compose -f docker-compose.yml -f {files} """.format(
            env_vars = env_vars, files = compose_files_flags
        )


        self._logger.debug("Running docker-compose command : {}".format(command))
        if self._logger.getEffectiveLevel() == logging.DEBUG:
            self._do(docker_comose_cmd + "config")

        return self._do(docker_comose_cmd + command)

    @property
    def _conn(self):
        if not self._fabric_connection:
            self._fabric_connection = fabric.Connection('localhost')
        return self._fabric_connection

    @property
    def _dotenv_file(self):
        return os.path.join(self._dir,'.env')

    @property
    def _configs(self):

        if not self._dir:
            return {}

        # create copy of default config
        configs = self._COMPOSE_ENV_VARS_.copy()

        # update using the dotenv file
        dotenv = utils.dotenv_to_dict(self._dotenv_file)
        configs.update(dotenv)

        # update using the env vars
        for k in configs.keys():
            if k in os.environ:
                configs[k] = os.environ[k]

        return configs

    def _check_configs(self):

        configs = self._configs

        for d in ["SNAP_HOME", "SHARED_DIR"]:
            configs[d] = os.getenv(d,"")

        unset_keys = dict(filter(lambda i : i[1] == "", configs.items()))

        if len(unset_keys):

            utils.do_exit(msg="""
                Please set the following environment variables
                 or add them to the .env file : {f}
                 
                 {k}
                  
                 """.format(k=list(unset_keys.keys()), f=self._dotenv_file)
                 )

        return configs


    def _do(self,it):
        result = ""
        cwd = os.getcwd()
        try:
            os.chdir(self._dir)
            self._logger.debug("[{d}] Doing: {it}".format(it=it, d=self._dir))
            result = self._conn.local(it).stdout
        except KeyboardInterrupt as e:
            self._logger.error("Keyboard Interupt : {}".format(e))
        except Exception as e:
            pass
        finally:
            os.chdir(cwd)

        return result

    @property
    def _compose_files(self):
        """
        Get docker-compose files
        :return: dictionary of {"parent_dir":"path/to/docker-compose.yml"}
        """
        compose_files = glob.glob(os.path.join(self._dir, '*', 'docker-compose.y*ml'))
        compose_directories = {os.path.basename(os.path.dirname(file)): file for file in compose_files}
        return compose_directories


    def _check_dependancies(self):
        return self._check_docker_compose_exists() \
                and self._check_docker_running()

    def _check_docker_compose_exists(self):
        if shutil.which("docker") == None:
            self._logger.error("""
                Could not find `docker` in your PATH.
                Please download docker:

                    https://docs.docker.com/install/

                """
                  )
            return False

        if shutil.which("docker-compose") == None:
            self._logger.error("""
                Could not find `docker-compose` in your PATH.
                Please download docker-compose:

                    https://docs.docker.com/compose/install/

                """
                  )
            return False
        return True

    def _check_docker_running(self):
        try:
            fabric.Connection('localhost').local("docker ps > /dev/null")
            return True
        except invoke.exceptions.UnexpectedExit as e:
            self._logger.error("""
                Docker doesn't seem to be running: 

                    {}
            """.format(e.result.stderr))
            return False
        except Exception as e:
            self._logger.error("""
                Unexpected exception when checking for Docker:
                    
                    {}
            """.format(e))
            return False

    def __repr__(self):
        if self._cluster_directory:
            return "<SNAPCuster dir={}>".format(self._dir)
        else:
            return "<SNAPCluster>"

    def _create_snap_docker_compose_dir(self,dest):
        """
        Creates the snap-server directory inside the cluster directory
        :param dest: where to copy the snap-server dir to
        :return: True if successfully created
        """

        compose_dir = os.path.join(utils.get_package_dir(),"snap-docker-compose")
        shutil.copytree(compose_dir,dest)
        return True


    def _create_snap_notebooks_dir(self,dest):
        """
        Creates the snap-notebook directory inside the cluster directory
        :param dest: where to copy the snap-notebooks dir to
        :return: True if successfully created
        """

        notebooks_dir = os.path.join(utils.get_package_dir(),"snap-notebooks")
        shutil.copytree(notebooks_dir,dest)
        return True

    def _create_snap_scriptedforms_dir(self,dest):
        """
        Creates the snap-server directory inside the cluster directory
        :param dest: where to copy the snap-server dir to
        :return: True if successfully created
        """

        compose_dir = os.path.join(utils.get_package_dir(),"snap-scriptedforms")
        shutil.copytree(compose_dir,dest)
        return True



if __name__ == '__main__':
    import fire
    fire.Fire(SNAPCluster)
