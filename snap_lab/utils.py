import os


def do_exit(msg=""):
    if msg:
        print(msg)
    import sys; sys.exit()

def getenv_or_exit(env_var,msg):
    if env_var in os.environ:
        return os.environ[env_var]
    else:
        print(msg.format(env_var))
        do_exit()

from urllib.request import urlopen
from io import BytesIO
import tarfile

def fetch_gitlab_tar(org_url,repo_name,branch,dest):

    while org_url.endswith('/'):
        org_url = org_url[:-1]

    TAR_URL = "{org_url}/{repo_name}/-/archive/{branch}/{repo_name}-{branch}.tar.gz".format(**locals())

    fetch_and_untar(TAR_URL, dest, "{repo_name}-{branch}".format(**locals()))

def fetch_and_untar(which_url, path, remove_prefix):
    """
    Fetch a URL and untar it.
    :param which_url: URL of the downloadable tar file
    :param path: where to extract the tar
    :param remove_prefix: provided string will be removed from resulting filepaths
    :return: None
    """

    def get_members(tar, prefix):
        if not prefix.endswith('/'):
            prefix += '/'
        offset = len(prefix)
        for tarinfo in tar.getmembers():
            if tarinfo.name.startswith(prefix):
                tarinfo.name = tarinfo.name[offset:]
            yield tarinfo

    tar = tarfile.open(mode='r', fileobj=BytesIO(urlopen(which_url).read()))
    tar.extractall(path=path, members=get_members(tar, prefix=remove_prefix))
    tar.close()

def dotenv_to_dict(path):
    with open(path,mode='r') as f:
        lines = f.read().split('\n')
        return dict(filter(lambda l: len(l) == 2, map(lambda x: x.split('='), lines)))

def get_package_dir():
    """Returns the directory that contains this package
        * If used as regular python module, this returns the parent of __file__
        * If used as executable, this returns the temporary directory used by the executable
    """
    import sys,os
    if getattr(sys, 'frozen', False):
        # running in a frozen bundle
        parent_dir = sys._MEIPASS
    else:
        # running live
        parent_dir = os.path.dirname(__file__)
    return parent_dir


import os
class FileBrowser(object):
    def __init__(self):
        self.path = os.getcwd()
        self._update_files()

    def _update_files(self):
        self.files = list()
        self.dirs = list()
        if (os.path.isdir(self.path)):
            for f in os.listdir(self.path):
                ff = self.path + "/" + f
                if os.path.isdir(ff):
                    self.dirs.append(f)
                else:
                    self.files.append(f)

    def widget(self):
        import ipywidgets as widgets
        box = widgets.VBox()
        self._update(box)
        return box

    def _update(self, box):

        def on_click(b):
            if b.description == '..':
                self.path = os.path.split(self.path)[0]
            else:
                self.path = self.path + "/" + b.description
            self._update_files()
            self._update(box)

        buttons = []
        if self.files:
            button = widgets.Button(description='..', background_color='#d0d0ff')
            button.on_click(on_click)
            buttons.append(button)
        for f in self.dirs:
            button = widgets.Button(description=f, background_color='#d0d0ff')
            button.on_click(on_click)
            buttons.append(button)
        for f in self.files:
            button = widgets.Button(description=f)
            button.on_click(on_click)
            buttons.append(button)
        box.children = tuple([widgets.HTML("<h2>%s</h2>" % (self.path,))] + buttons)
