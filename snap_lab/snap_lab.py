#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from snap_lab.snap_cluster import SNAPCluster
from snap_lab.snap_box import SNAPBox
from snap_lab.snap_client import SNAPClient
import snap_lab.utils as utils
import fire
import logging
import os, time , json

"""Main module."""

loggingFormat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=loggingFormat, level=logging.INFO)
logger = logging.Logger("SNAPLab")

class SNAPLab(object):
    """
    \n\n\nCommand line tools for interacting
    with SNAP clusters

    cluster - create, start, stop a cluster
    sql - connect to a SNAP cluster and run sql
    box - download and install snap packages

    """

    def __init__(self):
        """
        CLI for interacting with SNAP Clusters and packages
        """
        pass

    def demo(self):
        import pip

        pip.main(["install", "scriptedforms"])
        import scriptedforms
        scripted_forms_dir = os.path.join(utils.get_package_dir(),"snap-scriptedforms")

        scriptedforms.load(os.path.join(scripted_forms_dir,"create_cluster.md"))
        return

    def cluster(self):
        """
        Commands related to managing a SNAP cluster
        optional flags:
            --dir /path/to/snap/cluster/dir
            --debug=True
        """
        return SNAPCluster

    def box(self):
        """
        Commands related to managing SNAP packages
        """
        return SNAPBox

    def download(self, package, destination):
        print(f"Downloading package {package} to {destination}")
        box = SNAPBox(destination)
        box.download(package)
        print(f"Finished downloading package {package} to {destination}")

    def install(self,package_dir, snap_host, snap_port=10000, indexes = True, data_path=None, verify=True, database=None):
        """
        Install a Package on a Snap Cluster
        :param package_dir:
        :param snap_host:
        :param snap_port:
        :param data_path:
        :param verify:
        :return:
        """
        box = SNAPBox(package_dir)
        client = SNAPClient(snap_host,snap_port)

        if is_installed(box, client):
            print(f"SNAPBox {box} is already installed on Snap at {client}")
            return

        do_indexes_ddl = indexes

        print(f"Installing SNAPBox {box} on Snap at {client}, Indexes: {do_indexes_ddl}")

        install(box=box
                , client=client
                , data_path=data_path
                , indexes=do_indexes_ddl
                , database=database)

        if verify:
            print("Checking sizes of tables")
            for table_name in box._create_tables.keys():
                if table_name not in box._create_olap_indexes.keys():
                    r = client.sql('select count(*) as count from {}'.format(table_name))
                    number_of_rows_in_table = r[0]['count']
                    print(f"Number of rows in {table_name} : {number_of_rows_in_table}")

    def is_installed(self, package_dir, snap_host, snap_port=10000):
        """
        Check if package is installed
        :param package_dir:
        :param snap_host:
        :param snap_port:
        :return:
        """

        box = SNAPBox(package_dir)
        client = SNAPClient(snap_host, snap_port)
        status = "is not installed"
        if is_installed(box=box, client=client):
            status = "is installed"
        print(f"\nSNAPBox {box} {status} on {client}\n")

    def reindex(self, package_dir, snap_host, snap_port=10000, database=None, verify=True):

        box = SNAPBox(package_dir)
        client = SNAPClient(snap_host,snap_port)
        reindex_data(box
                     , client
                     , database=database)

        if verify:
            for index in box._insert_indexes.keys():
                r = client.sql(f"select count(*) as count from {index}")
                number_of_rows_in_index = r[0]['count']
                print(f"Number of rows in index {index} : {number_of_rows_in_index}")


    def uninstall(self, package_dir, snap_host, snap_port=10000, database=None):
        """
        Uninstall a SNAPBox
        :param package_dir:
        :param snap_host:
        :param snap_port:
        :param database:
        :return:
        """
        uninstall(SNAPBox(package_dir)
                  , SNAPClient(snap_host, snap_port)
                  , database)

    def sql(self,*query, host="localhost", port=10000, database=""):
        """
        Commands related to querying SNAP
        :param query: the sql query
        :return: json result of query
        """
        if not query:
            utils.do_exit(msg="""
                Please provide a query
                    For example: 
                        show tables
            """)

        return SNAPClient(host=host
                          ,port=port
                          ,database=database).sql(' '.join(map(str,query)))


def get_snap_app_metadata(dbparameters):

    if not dbparameters:
        return None

    pair = dbparameters.lstrip('(').rstrip(')')
    if not pair.startswith('json,'):
        return None

    jsonstr = pair[5:]
    params_dict = json.loads(jsonstr)
    return params_dict

def verify_install(box : SNAPBox, client : SNAPCluster) -> dict:
    table_verifications=dict(dict)
    for table_name in box._create_tables.keys():
        if table_name not in box._create_olap_indexes:
            count = client.sql('select count(*) as count from {}'.format(table_name))[0]['count']
            table_verifications[table_name] = count

    return table_verifications

def reindex_data(box : SNAPBox, client : SNAPClient, database : str = None):

    logger.warning("ReIndexing package {} using client {}".format(box._dir, client))

    app_name = os.path.basename(box._dir)

    if not database:
        database = app_name

    client.sql("use {}".format(database))

    for table_name,statement in box._insert_indexes.items():
        client.sql(statement)


def is_installed(box : SNAPBox, client : SNAPClient, database=None):

    app_name = os.path.basename(box._dir)

    if not database:
        database = app_name

    dbparameters = client.get_dbparameters(database)
    snap_app_metadata = get_snap_app_metadata(dbparameters)

    return snap_app_metadata and snap_app_metadata['snap_app'] == app_name

def uninstall(box : SNAPBox, client : SNAPClient, database=None):

    logger.info("Uninstalling package {} using client {}".format(box._dir, client))
    app_name = os.path.basename(box._dir)

    if not database:
        database = app_name

    dbparameters = client.get_dbparameters(database)
    snap_app_metadata = get_snap_app_metadata(dbparameters)

    if not snap_app_metadata:
        raise("Found database {} but it doesn't seem to be installed using SNAPLab.  Please remove it manually")


    if snap_app_metadata['snap_app'] != app_name:
        raise("Found database {} but its app_name ({}) doesn't match the name of this app ({})".format(database,snap_app_metadata['snap_app'],app_name))

    r = client.sql('use {}'.format(database))

    logger.info("Droping olap indexes: {}".format(box._drop_olap_indexes.keys()))

    for drop_oi in box._drop_olap_indexes.values():
        client.sql(drop_oi)

    logger.info("Droping star schemas: {}".format(box._drop_star_schemas.keys()))

    for drop_ss in box._drop_star_schemas.values():
        client.sql(drop_ss)

    logger.info("Droping base tables: {}".format(box._drop_tables.keys()))

    for drop in box._drop_tables.values():
        client.sql(drop)

    client.sql("drop database {}".format(database))

    logging.info("Finished uninstallation of {} using client {}".format(box,client))



def install(box : SNAPBox, client : SNAPClient, database=None, data_path=None, indexes = True):
    """
    install a snap package into a cluster
    data_path should be the path to the downloaded package as seen by the
    workers AND thriftserver. This assumed they are sharing this location.
    """

    logger.info("Installing package {} using client {}".format(box._dir, client))

    app_name = os.path.basename(box._dir)

    if not database:
        database = app_name

    # data_path should be the location of the snapbox on
    # the snap thrift server. In case of docker, /shared
    # is mounted to snap, spark, and notebook containers

    if not data_path:
        data_path = os.path.join("/data/shared/", app_name)

    box._data_path = data_path

    # TODO: Get version from source of truth
    snap_lab_version = "0.1.0"

    number_of_tables = len(box._create_tables)

    dbproperties_dict = {"snap_app":app_name
                        ,"snap_lab_version":snap_lab_version
                        ,"number_of_tables":number_of_tables}

    client.sql("""
    create database {database}
    comment "Created by SNAPLab on {when}" 
    with dbproperties (json='{dbproperties_json}')
    """.format(database=database
               ,when=time.ctime()
               ,dbproperties_json=json.dumps(dbproperties_dict)))

    r = client.sql('use {}'.format(database))

    box._make_index_dirs()

    logger.info("Creating base tables: {}".format(box._create_tables.keys()))

    for create in box._create_tables.values():
        client.sql(create)

    if indexes:

        logger.info("Creating star schemas: {}".format(box._create_star_schemas.keys()))

        for create_ss in box._create_star_schemas.values():
            client.sql(create_ss)

        logger.info("Creating olap indexes: {}".format(box._create_olap_indexes.keys()))

        for create_oi in box._create_olap_indexes.values():
            client.sql(create_oi)

    logging.info("Finished installation of {} using client {}".format(box,client))


def create_parquet_tables(client, old_dbname, new_dbname, parquet_path=None):
    """Creates parquet tables for each table in 'old_dbname' in a new database called 'new_dbname'
        Files are create at 'parquet_path/table_name'
    """
    tables = [x['tableName'] for x in client.sql("use {old_dbname}; show tables".format(old_dbname=old_dbname))]

    client.sql("create database if not exists {}".format(new_dbname))

    for name in tables:

        partition_by_list = ','.join(client.get_partition_columns(name))
        if partition_by_list:
            partition_by_list = f"PARTITIONED BY ({partition_by_list})"

        if not parquet_path:
            import os
            parquet_path = os.path.join(os.path.dirname(client.get_table_location(name)),"parquet")

        ddl = f"""        
        drop table if exists {new_dbname}.{name};  
        CREATE TABLE {new_dbname}.{name} 
            USING PARQUET
            OPTIONS ('compression'='snappy') {partition_by_list} 
            LOCATION "{parquet_path}/{name}"
            COMMENT "Table Created by SnapLab"
            AS select * from {old_dbname}.{name}
        """
        print(ddl)
        client.sql(ddl)
    return

def main():
    return fire.Fire(SNAPLab)

if __name__ == "__main__":
    main()
