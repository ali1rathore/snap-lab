#!/usr/bin/env python3
import fire
import os
import logging
import snap_lab.utils as utils
import json
SNAP_BOX_DIR="SNAP_BOX_DIR"

class SNAPBox(object):

    _DDL_DIR='ddl'
    _SQL_DIR='sql'
    _SOURCE_DATA_DIR='source_data'
    _INDEX_DATA_DIR='index_data'

    _FILE_TYPES = [
        'create_table'
        , 'create_star_schema'
        , 'create_olap_index'
    ]

    def __repr__(self):
        return f"SNAPBox@{self._dir}"

    def __init__(self,dir):

        self._dir = dir
        if not self._dir:
            self._dir = utils.getenv_or_exit(SNAP_BOX_DIR,"""
            Please set the {} environment variable to the desired snapbox directory
            """)

        self._dir = os.path.abspath(self._dir)

        self._data_path = self._source_data_dir
        self._logger = logging.getLogger('SNAPBox[{}]'.format(dir))


    def download(self,package: str):
        """Download SnapBox package to dir"""
        from tqdm import tqdm
        import requests
        import math
        done = False

        destination = self._dir

        if os.path.exists(destination):
            self._logger.error(f"Cannot download package {package} to {destination}. Destnnation {destination} already exists")

        url = f"https://gitlab.com/sparklinedata/snap-cubes/raw/master/snap-cubes/{package}"

        self._logger.debug(f"Downloading package {package} from url {url} to destination {destination}")

        basename = package
        if basename.endswith(".snapcube"):
            basename = basename.replace(".snapcube", "")
        utils.fetch_and_untar(url, destination, basename)

        return True

        # Streaming, so we can iterate over the response.
        r = requests.get(url, stream=True)

        # Total size in bytes.
        total_size = int(r.headers.get('content-length', 0));
        block_size = 1024
        wrote = 0

        with open(destination, 'wb') as f:
            for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size // block_size), unit='KB',
                             unit_scale=True):
                wrote = wrote + len(data)
                f.write(data)

        if total_size != 0 and wrote != total_size:
            self._logger.error(f"Error while downloadeding package {package}. Total size ({total_size}) does not match downloaded size ({wrote})")
        else:
            done = True

        return done

    def build(self):
        import quilt
        package_name = "sparky/" + os.path.basename(self._dir)
        build = quilt.build(package_name, self._dir)

    def publish(self):
        """
        Push SNAP Box package to quiltdata registry
        :return:
        """
        import quilt
        package_name = "sparky/" + os.path.basename(self._dir)
        push = quilt.push(package_name)

    def _make_index_dirs(self):
        indexed_tables = self._create_olap_indexes.keys()
        for t in indexed_tables:

            if not os.path.exists(self._index_data_dir):
                os.mkdir(self._index_data_dir)
            d = os.path.join(self._index_data_dir,t+'_index')
            if not os.path.exists(d):
                self._logger.info("Createing directory for index {} : {}".format(t,d))
                os.mkdir(d)

    def _format_statement(self,statement):
        try:
            statement = statement.replace('{data_path}',self._data_path)
        except Exception as e:
            raise Exception("Failed to format statement: {}".format(statement))
        return statement

    @property
    def _ddl_dir(self):
        return os.path.join(self._dir,self._DDL_DIR)

    @property
    def _sql_dir(self):
        return os.path.join(self._dir,self._SQL_DIR)

    @property
    def _source_data_dir(self):
        return os.path.join(self._dir,self._SOURCE_DATA_DIR)

    @property
    def _index_data_dir(self):
        return os.path.join(self._dir,self._INDEX_DATA_DIR)

    @property
    def _ddl_files(self):
        ddl_files = os.listdir(self._ddl_dir)
        files = {extension : list(filter(lambda s:s.endswith(extension),ddl_files))
                 for extension in self._FILE_TYPES
                }
        return files

    def _get_ddl(self, statement_type):
        ddls = {}
        for file in self._ddl_files[statement_type]:
            table_name = file.split('.')[0]
            with open(os.path.join(self._ddl_dir, file)) as f:
                ddl_str = f.read()
                ddl_str = self._format_statement(ddl_str)
                ddls[table_name] = ddl_str

        return ddls

    def query(self,name):
        if name not in self.queries.keys():
            self._logger.error("No Query named {}".format(name))
            return ""
        return self.queries[name]

    @property
    def queries(self):
        files = os.listdir(self._sql_dir)
        sql_files = filter(lambda s:s.endswith('sql'),files)

        parameters = {}
        parameters_txt = open(os.path.join(self._sql_dir,'parameters.txt')).read() if 'parameters.txt' in files else ''
        if parameters_txt:
            parameters_lines = parameters_txt.split('\n')
            try:
                parameters = dict(map(lambda r: r.split('='), parameters_lines))
            except Exception as e:
                self._logger.error("""
                Could not parse the parameters file (parameters.txt):
                
                    {}
                
                """.format(parameters_txt))

        _queries = {}

        for file in sql_files:
            query_name = file.split('.')[0]
            with open(os.path.join(self._sql_dir,file)) as f:
                query_txt = f.read()
                query_txt = query_txt.format(**parameters)
                _queries[query_name] = query_txt
        return _queries

    def _get_results_hash_dict(self):
        files = os.listdir(self._sql_dir)
        results = {}
        results_txt = open(os.path.join(self._sql_dir,'results.json')).read() if 'results.json' in files else ''
        if results_txt:
            results = json.loads(results_txt)
        return results

    def _write_result_hash_dict(self,results_hash_dict):
        with open(os.path.join(self._sql_dir,'results.json'), 'w') as f:
            f.write(json.dumps(results_hash_dict))

    @property
    def _create_tables(self):
        return self._get_ddl('create_table')

    @property
    def _create_star_schemas(self):
        return self._get_ddl('create_star_schema')

    @property
    def _create_olap_indexes(self):
        return self._get_ddl('create_olap_index')

    @property
    def _drop_tables(self):
        table_names = map(lambda s:s.split('.')[0],self._ddl_files['create_table'])
        return {table_name:"drop table if exists {}".format(table_name) for table_name in table_names}

    @property
    def _drop_star_schemas(self):
        table_names = map(lambda s:s.split('.')[0],self._ddl_files['create_star_schema'])
        return {table_name:"drop star schema on {}".format(table_name) for table_name in table_names}

    @property
    def _drop_olap_indexes(self):
        table_names = map(lambda s:s.split('.')[0],self._ddl_files['create_olap_index'])
        return {table_name:"drop olap index {t}_index on {t}".format(t=table_name) for table_name in table_names}

    @property
    def _insert_indexes(self):
        table_names = map(lambda s:s.split('.')[0],self._ddl_files['create_olap_index'])
        return {table_name:"insert olap index {t}_index of {t}".format(t=table_name) for table_name in table_names}


if __name__ == '__main__':
    fire.Fire(SNAPBox)
