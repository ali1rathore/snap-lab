#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', ]

with open("requirements.txt") as requirements_file:
    requirements_lines = requirements_file.read().split('\n')
    requirements_list = [str(line) for line in requirements_lines
                         if not line.lstrip().startswith("#")
                         and line is not '']

    requirements += requirements_list

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Ali R",
    author_email='ali1rathore@gitlab',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    description="Tools for developing, testing, and demoing Spark apps",
    entry_points={
        'console_scripts': [
            'snap_lab=snap_lab.snap_lab:main',
        ],
    },
    install_requires=requirements,
    license="Apache Software License 2.0",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='snap_lab',
    name='snap_lab',
    packages=find_packages(include=['snap_lab']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/SparklineData/Public/snap-lab',
    version='0.1.0',
    zip_safe=False,
)
